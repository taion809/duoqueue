package main

type Duoqueue struct {
	left  *Stack
	right *Stack
}

func NewDuoqueue() *Duoqueue {
	d := Duoqueue{left: &Stack{}, right: &Stack{}}

	return &d
}

func (d *Duoqueue) Enqueue(n int) {
	d.left.Push(n)
}

func (d *Duoqueue) Dequeue() int {
	d.shiftRight()
	n := d.left.Pop()
	d.shiftLeft()

	return n
}

func (d *Duoqueue) shiftRight() {
	for i := d.left.Size(); i > 1; i-- {
		n := d.left.Pop()
		d.right.Push(n)
	}
}

func (d *Duoqueue) shiftLeft() {
	for i := d.right.Size(); i > 0; i-- {
		n := d.right.Pop()
		d.left.Push(n)
	}
}

func (d *Duoqueue) Size() int {
	return d.left.Size()
}

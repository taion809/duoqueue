package main

import "testing"

func TestSizeReturnsCorrect(t *testing.T) {
	duoq := NewDuoqueue()

	ns := []int{0, 1, 2, 3, 4, 5, 6}
	for _, v := range ns {
		duoq.Enqueue(v)
	}

	if size := duoq.Size(); size != len(ns) {
		t.Errorf("Stack size inconsistent: expected %d got %d\n", len(ns), size)
	}
}

func TestDequeReturnsLifo(t *testing.T) {
	duoq := NewDuoqueue()

	ns := []int{0, 1, 2, 3, 4, 5, 6}
	for _, v := range ns {
		duoq.Enqueue(v)
	}

	for i := 0; i < duoq.Size(); i++ {
		n := duoq.Dequeue()

		if n != ns[i] {
			t.Errorf("Dequeued value not correct: got %d expected %n\n", n, ns[i])
		}
	}
}

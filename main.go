package main

import (
	"fmt"
)

func main() {
	fmt.Println("Duoqueue Test")

	in := []int{0, 1, 2, 3, 4}

	fmt.Printf("Using %v expecting %v\n", in, in)

	duoq := NewDuoqueue()

	for _, v := range in {
		duoq.Enqueue(v)
	}

	for i := duoq.Size(); i > 0; i-- {
		fmt.Println("Pop: ", duoq.Dequeue())
	}
}

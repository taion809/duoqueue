package main

import (
	"testing"
)

func TestStackSize(t *testing.T) {
	s := &Stack{}

	ns := []int{0, 1, 2, 3, 4, 5}

	for i := 0; i < len(ns); i++ {
		s.Push(ns[i])
	}

	size := s.Size()

	if size != len(ns) {
		t.Errorf("Stack size inconsistent: expected %d got %d\n", len(ns), size)
	}
}

func TestPopStack(t *testing.T) {
	s := &Stack{}

	input := []int{0, 1, 2, 3, 4, 5}
	expected := []int{5, 4, 3, 2, 1, 0}

	for i := 0; i < len(input); i++ {
		s.Push(input[i])
	}

	for i := s.Size(); i > 0; i-- {
		n := s.Pop()
		e := expected[0]
		expected = expected[1:]

		if e != n {
			t.Errorf("Popped node %v does not match expected output %v\n", n, e)
		}
	}
}

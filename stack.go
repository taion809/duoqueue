package main

type Stack struct {
	nodes []int
}

func (s *Stack) Push(n int) {
	s.nodes = append(s.nodes, n)
}

func (s *Stack) Pop() int {
	idx := len(s.nodes) - 1
	n := s.nodes[idx]
	s.nodes = s.nodes[:idx]

	return n
}

func (s *Stack) Size() int {
	return len(s.nodes)
}
